package com.cmwstudios.creativefreedommod.obj.cuser;

public enum Rank {
    NON_OP,OP,MOD,ADMIN,OWNER
}

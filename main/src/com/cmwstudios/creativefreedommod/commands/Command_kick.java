package com.cmwstudios.creativefreedommod.commands;

import com.cmwstudios.creativefreedommod.util.CommandUtil;
import com.cmwstudios.creativefreedommod.util.PUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_kick implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
//            if (commandSender instanceof commandSender
//        commandSender commandSender = (commandSender) commandSender;
        if (commandSender instanceof Player) {
            if (!PUtil.hasPermission((Player) commandSender, "com.cmwstudios.creativefreedommod.moderation.kick")) {
                commandSender.sendMessage(CommandUtil.noRank);
                return true;
            }
        }
        if (!Bukkit.getOnlinePlayers().contains(commandSender.getServer().getPlayer(args[0]))) {
            commandSender.sendMessage(CommandUtil.notFound);
            return false;
        }
        String broad = "";
        for (int i = 1; i < args.length; i++) {
            broad += args[i] + " ";
        }
        commandSender.getServer().getPlayer(args[0]).kickPlayer(ChatColor.RED + "Kicked!\nReason " + broad);
        return true;
    }
}

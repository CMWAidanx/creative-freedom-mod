package com.cmwstudios.creativefreedommod.commands;

import com.cmwstudios.creativefreedommod.util.CommandUtil;
import com.cmwstudios.creativefreedommod.util.PUtil;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_ban implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        if (!PUtil.hasPermission(player, "com.cmwstudios.creativefreedommod.moderation.ban")) {
            player.sendMessage(CommandUtil.noRank);
            return true;
        }
        if (!Bukkit.getOnlinePlayers().contains(player.getServer().getPlayer(args[0]))) {
            player.sendMessage(CommandUtil.notFound);
            return false;
        }
        String broad = "";
        for (int i = 1; i < args.length; i++) {
            broad += args[i] + " ";
        }
        Bukkit.getBanList(BanList.Type.NAME).addBan(args[0], ChatColor.RED + broad, null, null);
        return true;
    }
}
package com.cmwstudios.creativefreedommod.clog;

import org.bukkit.ChatColor;

public class CLogger {
    // It's pronounced C Logger, not Clogger.
    public void logMessage(String msg) {
        System.out.println(ChatColor.AQUA + "[CFM] {Message] " + msg);
    }
    public void logWarning(String msg) {
        System.out.println(ChatColor.GOLD + "[CFM] [Warning]" + msg);
    }
    public void logError(String msg) {
        System.out.println(ChatColor.RED + "[CFM] [Error] " + msg);
    }
}

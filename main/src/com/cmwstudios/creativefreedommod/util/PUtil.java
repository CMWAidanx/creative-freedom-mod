package com.cmwstudios.creativefreedommod.util;

import org.bukkit.entity.Player;

public class PUtil {
    public static boolean hasPermission(Player player, String permission) {
        return player.hasPermission(permission);
    }
}

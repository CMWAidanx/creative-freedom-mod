package com.cmwstudios.creativefreedommod.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class CUtil {
    public static void broadcastMsg(String msg, ChatColor chatColor) {
        Bukkit.broadcastMessage(chatColor + msg);
    }
    public static void adminAction(String name, String action, ChatColor chatColor) {
        broadcastMsg(name + ": " + action, chatColor);
    }
}

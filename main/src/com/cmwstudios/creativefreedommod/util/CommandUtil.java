package com.cmwstudios.creativefreedommod.util;

import org.bukkit.ChatColor;

public class CommandUtil {
    public static final String noRank = ChatColor.RED + "I'm sorry, you do not seem to have permission to do that";
    public static final String notFound = ChatColor.RED + "I couldn't find that player, are they online?";
}

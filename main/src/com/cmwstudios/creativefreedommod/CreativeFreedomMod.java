package com.cmwstudios.creativefreedommod;

import com.cmwstudios.creativefreedommod.clog.CLogger;
import com.cmwstudios.creativefreedommod.commands.Command_ban;
import com.cmwstudios.creativefreedommod.commands.Command_kick;
import com.cmwstudios.creativefreedommod.listeners.JoinListener;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class CreativeFreedomMod extends JavaPlugin {
    CLogger c = new CLogger();
    private File customConfigFile;
    private FileConfiguration customConfig;
    @Override
    public void onEnable() {
        c.logMessage("Enabling Creative Freedom Mod");
        c.logMessage("Checking config and creating new one if none exists");
        createCustomConfig();
        c.logMessage("Registering commands");
//        getCommand("aboutme").setExecutor(new Command_aboutme());
//        getCommand("opall").setExecutor(new Command_opall());
        getCommand("ban").setExecutor(new Command_ban());
        getCommand("kick").setExecutor(new Command_kick());
        c.logMessage("Registering listeners");
//        getServer().getPluginManager().registerEvents(new CommandBlocker(), this);
        getServer().getPluginManager().registerEvents(new JoinListener(), this);
    }
    public FileConfiguration getCustomConfig() {
        return this.customConfig;
    }

    private void createCustomConfig() {
        customConfigFile = new File(getDataFolder(), "admins.yml");
        if (!customConfigFile.exists()) {
            customConfigFile.getParentFile().mkdirs();
            saveResource("admins.yml", false);
        }

        customConfig = new YamlConfiguration();
        try {
            customConfig.load(customConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onDisable() {
        c.logMessage("Disabling Creative Freedom Mod");
    }

}

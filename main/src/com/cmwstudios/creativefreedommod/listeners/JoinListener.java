package com.cmwstudios.creativefreedommod.listeners;

import com.cmwstudios.creativefreedommod.util.CUtil;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {
    public void onPlayerJoin(PlayerJoinEvent event) {
        CUtil.broadcastMsg(ChatColor.GRAY + "[" + ChatColor.DARK_GREEN + "+" + ChatColor.GRAY + "] " + ChatColor.AQUA + event.getPlayer().getName(), ChatColor.RESET);

    }
}
